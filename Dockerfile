FROM alpine:3.10

# Install Sauce Connect Proxy for remote browsers provided by Sauce Labs
ENV GLIBC_VERSION=2.29-r0
RUN apk add --no-cache libressl && \
    wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub && \
    wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/$GLIBC_VERSION/glibc-$GLIBC_VERSION.apk && \
    apk add --no-cache --virtual .sc-deps \
      ca-certificates \
      glibc-$GLIBC_VERSION.apk && \
    rm glibc-$GLIBC_VERSION.apk
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib

ENV SC_VERSION=4.5.4
RUN mkdir -p /opt && \
    wget -O - https://saucelabs.com/downloads/sc-$SC_VERSION-linux.tar.gz | tar -xzf - -C /opt
ENV PATH=$PATH:/opt/sc-$SC_VERSION-linux/bin

ENTRYPOINT ["sc", "--readyfile", "/tmp/sc-ready"]

HEALTHCHECK --start-period=30s CMD ["[", "-e", "/tmp/sc-ready", "]"]
