# Sauce Connect Proxy

Sauce Connect is a proxy server for secure connections between Sauce Labs virtual and a applications/websites behind a corporate firewall. See [Sauce Connect Proxy](https://wiki.saucelabs.com/display/DOCS/Sauce+Connect+Proxy) for details.

This project produces a Docker image with the `sc` binary on an [alpine](https://hub.docker.com/_/alpine/) base image.

## Using this image

```sh
$ docker container run \
  --env SAUCE_USERNAME=$YOUR_SAUCE_USERNAME \
  --env SAUCE_ACCESS_KEY=$YOUR_SAUCE_ACCESS_KEY \
  --publish 4445:4445 \
  registry.gitlab.com/oildex/library/sc-proxy:4.5.3
```

Use the `--help` flag to get a full list of options.

```sh
$ docker container run \
  registry.gitlab.com/oildex/library/sc-proxy:4.5.3 \
  --help
```
